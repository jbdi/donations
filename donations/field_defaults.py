# conding: utf-8

field_blank_null = {
    'null': True,
    'blank': True,
}

field_blank_null_empty = {
    'default': '',
    'null': True,
    'blank': True,
}
