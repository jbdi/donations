# coding: utf-8
from django.conf.urls import url, include

urlpatterns = [
    url(r'^login/$', 'custom_auth.views.login', name='login'),
    url(r'^logout/$', 'custom_auth.views.logout', name='logout'),

    # url(r'^$', 'custom_auth.views.home'),
    url(r'^email-sent/', 'custom_auth.views.validation_sent'),
    url(r'^login/$', 'custom_auth.views.login'),
    url(r'^logout/$', 'custom_auth.views.logout'),
    url(r'^done/$', 'custom_auth.views.done', name='done'),
    url(r'^ajax-auth/(?P<backend>[^/]+)/$', 'custom_auth.views.ajax_auth',
        name='ajax-auth'),
    url(r'^email/$', 'custom_auth.views.require_email', name='require_email'),
    url(r'', include('social.apps.django_app.urls', namespace='social'))
]

