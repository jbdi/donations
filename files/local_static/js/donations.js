var io = io.connect('ws://' + socketio_host);
var queueDonations = [];

io.on('open', function () {
    // console.log('opened');
});

var stopReader = function (queue) {
    clearInterval(queue);
};
var startReader = function (func) {
    return setInterval(func, 1000);
};
var startListeningWidget = function () {
    var s = startReader(function () {
        if (queueDonations.length) {
            stopReader(s);
            renderWidget();
            var audio = document.getElementById('player');
            audio.addEventListener('loadedmetadata',function(){
                audio.setAttribute('data-time', audio.duration);
                console.log('settimeout is ' + audio.duration * 1000);
                setTimeout(clearWidget, audio.duration * 1000);
            },false);
        } else {
            // console.log('awaiting');
        }
    });
    return s;
};
startListeningWidget();

// хранить этот список в localstorge
var renderWidget = function () {
    if (!queueDonations || !queueDonations.length) return;

    console.log('render');
    var donation = queueDonations.shift();
    var widgetArea = document.getElementById('widget');

    var donationTemplate = _.template(
        "<div><img src='http://www.stayfocusd.com/assets/img/omg.gif' width='100'></div>" +
        "<div class='title'><%- title %> - <%- amount %></div>" +
        "<div class='text'><%- text %></div>" +
        "<audio id='player' autoplay><source src='<%- speech %>' type='audio/mp3'></audio>"
    );
    var donationHtml = donationTemplate(donation);
    widgetArea.innerHTML = donationHtml;

    var audio = document.getElementById('player');
    console.log(audio);
    audio.addEventListener('loadedmetadata',function(){
        audio.setAttribute('data-time', audio.duration);
    }, false);
};

var clearWidget = function () {
    console.log('cleanwidget');
    document.getElementById('widget').innerHTML = '';
    startListeningWidget();
};


// передаем токен юзера для авторизации
// io.emit('authorize', { auth_token: 'auth_token' });
var changeStatus = function (donation) {
    // - если нет ошибок воспроизведения, сети и других, что могли повлиять на отображение, то меняем статус
    donation.status = 3; // 3 - прослушано
    return donation;
};

// NOTE: подписывать ли каждый запрос токеном? Или достаточно момента инициализации?
io.emit('set-tokens', {'csrfmiddlewaretoken': '{{ csrf_token }}'});

// - получение списка донатов
io.on('queue-donation', function (data) {
    // клонирование объекта
    var donationsData = JSON.parse(JSON.stringify(data))
    if (donationsData && donationsData.donations && donationsData.donations.length) {
        // console.log(donationsData.donations);
        queueDonations = queueDonations.concat(donationsData.donations);
    }

    // - какая-то функция по событию меняет статус доната
    // - обновленный (или нет) список донатов с измененными статусами уходит обратно
    if (donationsData.donations.length) {
        donationsData.donations.forEach(function(donation) {
            donation = changeStatus(donation);
        });
    }

    // NOTE:  сделать механизм синхронизации донатов
    //        сейчас донаты отошлются с новым статусом на сервер, сервер обновит их и будет пытаться прислать новые
    //        добавить сохранение очереди донатов в localstorage
    // console.log(queueDonations);
    io.emit('queue-donation', donationsData);
});