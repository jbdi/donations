# coding: utf-8

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.utils.timezone import now

from donations.field_defaults import field_blank_null_empty, field_blank_null

from pyvona import Voice
import os


class statusChoices:
    NEW = 1
    SKIPPED = 2
    DONE = 3
    OFFLINE = 4
    ERROR = 5

    CHOICES = (
        (NEW, 'new'),
        (SKIPPED, 'skipped'),
        (DONE, 'done'),
        (OFFLINE, 'offline'),
        (ERROR, 'error'),
    )
    DEFAULT = NEW


def user_directory_path(instance, filename):
    # return 'user_{0}/{1}'.format(settings.MEDIA_ROOT, instance.user.id, filename)
    return '{0}/{1}'.format(settings.MEDIA_ROOT, now())


class SpeechFileField(models.FileField):

    pass

class Donate(models.Model):
    # user
    title = models.CharField(max_length=128, null=False)
    text = models.TextField(null=False)
    amount = models.PositiveSmallIntegerField(null=False)
    speech = SpeechFileField(upload_to=user_directory_path, null=True, blank=True)
    status = models.PositiveSmallIntegerField(choices=statusChoices.CHOICES, default=statusChoices.DEFAULT, null=False)
    # added_time
    # payment_status

    def __unicode__(self):
        return u'id:{} - {}'.format(self.id, self.title)

    @property
    def speech_uri(self):
        if not self.speech:
            return
        return os.path.join(settings.MEDIA_URL, os.path.split(self.speech.name)[1])


@receiver(post_save, sender=Donate)
def make_speech(sender, **kwargs):
    is_new = kwargs.get('created')
    instance = kwargs.get('instance')

    # if not is_new:
    if not is_new and instance.speech:
        return
    api = Voice('GDNAJPPSLUOXZA3ETU4A', 'iOSHV99iExGs7hiJz9MvMk2yVfq1tVWm+jTtL/GI')
    # api.codec = 'mp3'
    # api.speech_rate = 'x-fast'
    # api.fetch_voice(u'Hello!', './eng_test_text')
    api.codec = 'mp3'
    api.voice_name = 'Maxim'
    api.language = 'ru-RU'
    api.gender = 'Male'
    api.speech_rate = 'medium' #x-slow, slow, medium (default), fast, x-fast

    filepath = os.path.join(settings.MEDIA_ROOT, 'speech_{0}.{1}'.format(instance.id, api.codec))
    api.fetch_voice(instance.text, filepath)
    instance.speech = filepath
    instance.save()


