# coding: utf-8
import json
from operator import itemgetter
import os

from funcy import group_by

from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.text import mark_safe
from django.views.decorators.csrf import csrf_exempt

from index.models import Donate, statusChoices


def index(request):
    context = {'socketio_host': settings.SOCKETIO_HOST}
    return render(request, 'mainpage.jinja', context)

# async task?
def json_actual_donations(request):
    donations = Donate.objects.filter(status=statusChoices.NEW).values()[:10]
    donations = map(lambda d: media_filename(d), donations)

    context = {
        'status_choices': to_json(statusChoices.CHOICES),
        'data': to_json(list(donations)),
    }
    return render(request, 'index/json_actual_donations.jinja',
                  context, content_type="application/json")

# async task?
@csrf_exempt # NOTE: временно?
def donations_update_status(request):
    websocket_donations = json.loads(request.POST.get('donations'))
    donations_by_id = group_by(itemgetter('id'), websocket_donations)

    # NOTE: механизм кажется не совсем надежным
    # donations = Donate.objects.filter(id__in=donations_ids)
    for donation_id, donation in donations_by_id.items():
        Donate.objects.filter(id=donation_id).update(status=donation[0].get('status'))

    context = {
        'success': True,
    }
    return HttpResponse(to_json(context), content_type="application/json")

def to_json(some_dict):
    return mark_safe(json.dumps(some_dict))

def media_filename(data):
    for k, v in data.copy().items():
        if k == 'speech' and v:
            data[k] = os.path.join(settings.MEDIA_URL, os.path.split(v)[1])
    return data






