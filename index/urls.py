# coding: utf-8
from django.conf.urls import url, include

urlpatterns = [
    # url(r'', include('django_socketio.urls')),
    url(r'^/?json_actual_donations/?$', 'index.views.json_actual_donations', name='json_actual_donations'),
    url(r'^/?donations_update_status/?$', 'index.views.donations_update_status', name='donations_update_status'),
    url(r'^/?$', 'index.views.index', name='index'),
]
